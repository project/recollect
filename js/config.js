var _recollect_config = ((drupalSettings) => {
  return {
    area: drupalSettings.recollectConfig.area,
    locale: drupalSettings.path.currentLanguage,
    name: drupalSettings.recollectConfig.name,
  };
})(drupalSettings);