<?php

namespace Drupal\recollect\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Block\BlockPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'ReCollectWidgetBlock' block.
 *
 * @Block(
 *  id = "recollect_widget_block",
 *  admin_label = @Translation("recollect_widget_block"),
 * )
 */
class ReCollectWidgetBlock extends BlockBase implements BlockPluginInterface, ContainerFactoryPluginInterface {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a LanguageBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LanguageManagerInterface $language_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container
      ->get('language_manager'));
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'name' => 'calendar',
      'area' => 'Boston',
      'country_code' => 'us',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['name'] = [
      '#type' => 'select',
      '#title' => $this->t('Widget Type'),
      '#description' => $this->t('The type of widget to embed.'),
      '#default_value' => $this->configuration['name'],
      '#options' => [
        'calendar' => $this->t('Calendar'),
        'wizard' => $this->t('Waste Wizard'),
      ],
      '#weight' => '0',
    ];

    $form['area'] = [
      '#type' => 'textfield',
      '#title' => $this->t('City'),
      '#description' => $this->t('The city or area to display the widget for.'),
      '#default_value' => $this->configuration['area'],
      '#weight' => '0',
    ];

    $form['country_code'] = [
      '#type' => 'select',
      '#title' => $this->t('Country Code'),
      '#description' => $this->t('The two character country code. (not yet implemented)'),
      '#default_value' => $this->configuration['country_code'],
      '#options' => [
        'ca' => $this->t('Canada'),
        'us' => $this->t('United States'),
      ],
      '#weight' => '0',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['area'] = $form_state->getValue('area');
    $this->configuration['name'] = $form_state->getValue('name');
    $this->configuration['country_code'] = $form_state->getValue('country_code');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $langcode = $this->languageManager->getCurrentLanguage()->getId();
    $build = [];
    $build['#theme'] = 'recollect_widget_block';
    $build['#content'][] = NULL;
    $build['#langcode'] = $langcode;
    $build['#widget_type'] = $this->configuration['name'];
    $build['#attached']['drupalSettings'][] = 'recollectConfig';
    $build['#attached']['drupalSettings']['recollectConfig'] = [
      'area' => $this->configuration['area'],
      'name' => $this->configuration['name'],
    ];

    return $build;
  }

}
